/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.worawit.robotproject;

import java.util.Scanner;

/**
 *
 * @author werapan
 */
public class MainProgram {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        TableMap map = new TableMap(10, 10);
        Robot robot = new Robot(2, 2, 'x', map, 10);
        Bomb bomb = new Bomb(5, 5);
        map.addObj(new Tree(1, 1)); //lower priority
        map.addObj(new Tree(1, 2));
        map.addObj(new Tree(3, 2));
        map.addObj(new Tree(1, 0));
        map.addObj(new Tree(7, 7));
        map.addObj(new Tree(5, 4));
        map.addObj(new Tree(6, 4));
        map.addObj(new Tree(7, 4));
        map.addObj(new Tree(7, 5));
        map.addObj(new Tree(6, 7));
        map.addObj(new Fuel(2, 4, 20));
        map.addObj(new Fuel(3, 3, 10));
        map.addObj(new Fuel(8, 9, 5));
        map.setBomb(bomb);
        map.setRobot(robot); //higher priority
        while (true) {
            map.showMap();
            // W,a| N,w| E,d| S, s| q: quit
            char direction = inputDirection(sc);
            if (direction == 'q') {
                printByeBye();
                break;
            }
            robot.walk(direction);
        }

    }

    private static void printByeBye() {
        System.out.println("Bye Bye!!!");
    }

    private static char inputDirection(Scanner sc) {
        String str = sc.next();
        return str.charAt(0);
    }
}
